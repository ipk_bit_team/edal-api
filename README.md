# eDAL - electronic Data Archive Library #
# NOTE: this repository is deprecated #

### The e!DAL project was converted into a multi-build project. All three repositories (API, Server, Client) are now a part of the new [electronicDataArchiveLibrary repository](https://bitbucket.org/ipk_bit_team/electronicdataarchivelibrary) ###

### For more information about the e!DAL Project visit our [project website](http://edal.ipk-gatersleben.de/)###